package interfaces;

@FunctionalInterface
public interface IBytable {
    byte[] generateByteArray();
}
