import Objects.DNSAnswer;
import Objects.DNSHeaderFlags;
import Objects.DNSPacket;
import Objects.ResourceRecord;
import utils.Utility;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        try (DatagramSocket datagramSocket = new DatagramSocket(53)) {
            System.out.println("Listening on: " + datagramSocket.getLocalSocketAddress().toString());
            while (true) {
                byte[] data = new byte[4096];
                DatagramPacket datagramPacket = new DatagramPacket(data, data.length);
                datagramSocket.receive(datagramPacket);
                DNSPacket dnsPacket = new DNSPacket(data);
                System.out.println("/////////////////////////////Received Packet From(" + datagramPacket.getSocketAddress() + "):" + dnsPacket.toString(0));
//                System.out.println(Utility.bytesToHex(data));
                String ipAddressString = getRecordWithDomain(dnsPacket.getQUESTION().getQNAME()).getIpAddress();
                byte[] resultRData = Utility.getBytesOfIpAddressString(ipAddressString);
                dnsPacket.setANSWER(
                        new DNSAnswer()
                                .setCLASS(dnsPacket.getQUESTION().getQCLASS())
                                .setNAME(dnsPacket.getQUESTION().getQNAME())
                                .setRDLENGTH(resultRData.length)
                                .setRDATA(resultRData)
                                .setTTL(600)
                                .setTYPE(dnsPacket.getQUESTION().getQTYPE())
                ).getHEADER().setANCOUNT(1).getFLAGS().setQR(DNSHeaderFlags.QuestionResponse.RESPONSE).setRA(true);
                data = dnsPacket.generateByteArray();
                System.out.println("/////////////////////////////Sending Packet: To(" + datagramPacket.getSocketAddress() + ")" + dnsPacket.toString(0));
//                System.out.println(Utility.bytesToHex(data));
                //convert ip-Address to byte array if needed
                datagramPacket = new DatagramPacket(data, data.length, datagramPacket.getAddress(), datagramPacket.getPort());
                datagramSocket.send(datagramPacket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    private static String getDomainFromRequest(byte[] requestData) {
//        //byte 55 starts request domain
//        int counter = 55;
//        int length;
//        StringBuilder finalResultSB = new StringBuilder();
//        do {
//            length = Utility.bytesToInt(new byte[]{requestData[counter++], 0, 0, 0});
//            System.out.println(length);
//            byte[] stringData = Arrays.copyOfRange(requestData, counter, counter + length);
//            counter += length;
//            String string = Utility.getUTF8FromBytes(stringData);
////            System.out.println(string);
//            finalResultSB.append(string);
//        } while (length != 0);
//        return finalResultSB.toString();
//    }

    private static ResourceRecord getRecordWithDomain(String domain) {
        try {
            return Files
                    .readAllLines(Paths.get("address.txt"))
                    .parallelStream()
                    .map(ResourceRecord::getFromStringLine)
                    .filter(record -> record != null && record.equalsByDomain(domain))
                    .findAny()
                    .orElse(ResourceRecord.DEFAULT_INSTANCE);
        } catch (IOException e) {
            e.printStackTrace();
            return ResourceRecord.DEFAULT_INSTANCE;
        }
    }
}
