package enums;

public enum QueryClass {
    IN(1), CS(2), CH(3), HS(4);

    private int rawValue;

    QueryClass(int rawValue) {
        this.rawValue = rawValue;
    }

    public int getRawValue() {
        return rawValue;
    }

    public static QueryClass parse(int intValue) {
        switch (intValue) {
            case 1:
                return IN;
            case 2:
                return CS;
            case 3:
                return CH;
            case 4:
                return HS;
            default:
                return null;
        }
    }
}
