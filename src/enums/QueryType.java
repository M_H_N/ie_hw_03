package enums;

public enum QueryType {
    A(1), NS(2), MD(3), MF(4), CNAME(5), SOA(6), MB(7), MG(8), MR(9), NULL(10), WKS(11), PTR(12), HINFO(13), MINFO(14), MX(15), TXT(16);

    private int rawValue;

    QueryType(int rawValue) {
        this.rawValue = rawValue;
    }

    public int getRawValue() {
        return rawValue;
    }

    public static QueryType parse(int intValue) {
        switch (intValue) {
            case 1:
                return A;
            case 2:
                return NS;
            case 3:
                return MD;
            case 4:
                return MF;
            case 5:
                return CNAME;
            case 6:
                return SOA;
            case 7:
                return MB;
            case 8:
                return MG;
            case 9:
                return MR;
            case 10:
                return NULL;
            case 11:
                return WKS;
            case 12:
                return PTR;
            case 13:
                return HINFO;
            case 14:
                return MINFO;
            case 15:
                return MX;
            case 16:
                return TXT;
            default:
                return null;
        }
    }
}
