package utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class Utility {
    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes);
        buffer.flip();//need flip
        return buffer.getLong();
    }

    public static byte[] intToBytes(int value) {
        return new byte[]{
                (byte) (value >> 24),
                (byte) (value >> 16),
                (byte) (value >> 8),
                (byte) value};
    }

    public static byte[] intToBytes(int value, int bytesCount) {
        byte[] result = ByteBuffer.allocate(4).putInt(value).array();
        return Arrays.copyOfRange(result, result.length - bytesCount, result.length);
    }

    public static int bytesToIntPrime(byte... bytes) {
        switch (bytes.length) {
            case 0:
                return 0;
            case 1:
                return (bytes[0] & 0xff);
            case 2:
                return (((bytes[0] & 0xff) << 8) | ((bytes[1] & 0xff)));
            case 3:
                return (((bytes[0] & 0xff) << 16) | ((bytes[1] & 0xff) << 8) | ((bytes[2] & 0xff)));
            default:
                return Utility.bytesToInt(bytes);
        }
    }

    public static String generateIntStringOfByteArray(byte[] bytes) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < bytes.length; i++)
            stringBuilder.append(bytesToIntPrime(bytes[i])).append(i == bytes.length - 1 ? "" : ".");
        return stringBuilder.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static int bytesToInt(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.put(bytes);
        buffer.flip();
        return buffer.getInt();
    }

    public static boolean getBitFromByte(byte value, int index) {
        return ((value >> index) & 1) != 0;
    }

    public static int getIntFromBitsOfByte(byte value, int from, int to) { //don't include 'to' (from..<to)
        if (to <= from) return 0;
        boolean[] bits = toBooleanArray(value);
        byte[] bytes = toByteArray(Arrays.copyOfRange(bits, from, to));
//        int result = Utility.bytesToIntPrime(bytes);
//        System.out.println("getIntFromBitsOfByte_result: " + result);
        return Utility.bytesToIntPrime(bytes);
//        return 0;
    }

    public static boolean[] toBooleanArray(int intValue) {
        return toBooleanArray(intValue, 32);
    }

    public static boolean[] toBooleanArray(int intValue, int bitsCount) {
        boolean[] res = toBooleanArray(intToBytes(intValue));
        return Arrays.copyOfRange(res, res.length - bitsCount, res.length);
    }

    public static boolean[] toBooleanArray(byte byteValue) {
        return toBooleanArray(new byte[]{byteValue});
    }

    public static boolean[] toBooleanArray(byte[] bytes) {
        BitSet bits = BitSet.valueOf(bytes);
        boolean[] bools = new boolean[bytes.length * 8];
        for (int i = bits.nextSetBit(0); i != -1; i = bits.nextSetBit(i + 1)) {
            bools[i] = true;
        }
        return bools;
    }

    public static byte[] toByteArray(boolean[]... booleanArrays) {
        List<Boolean> booleanList = new ArrayList<>();
        for (int i = 0; i < booleanArrays.length; i++)
            for (int j = 0; j < booleanArrays[i].length; j++)
                booleanList.add(booleanArrays[i][j]);
        boolean[] booleanArray = new boolean[booleanList.size()];
        for (int i = 0; i < booleanList.size(); i++) booleanArray[i] = booleanList.get(i);
//        System.out.println(Arrays.toString(booleanArray));
        //        System.out.println(Arrays.toString(bytes));
        return toByteArray(booleanArray);
    }


    public static byte[] toByteArray(boolean... bools) {
        return toBytes(bools);
    }

    public static byte[] toBytes(boolean[] input) {
        byte[] toReturn = new byte[input.length / 8];
        for (int entry = 0; entry < toReturn.length; entry++) {
            for (int bit = 0; bit < 8; bit++) {
                if (input[entry * 8 + bit]) {
                    toReturn[entry] |= (128 >> bit);
                }
            }
        }

        return toReturn;
    }

    public static byte[] toByteArrayPrivate(boolean... bools) {
        BitSet bits = new BitSet(bools.length);
        for (int i = 0; i < bools.length; i++) {
            if (bools[i]) {
                bits.set(i);
            }
        }

        byte[] bytes = bits.toByteArray();
        if (bytes.length * 8 >= bools.length) {
            return bytes;
        } else {
            return Arrays.copyOf(bytes, bools.length / 8 + (bools.length % 8 == 0 ? 0 : 1));
        }
    }

    public static byte[] getBytesOfIpAddressString(String s) {
        if (s == null || s.isEmpty() || s.isBlank()) return new byte[0];
        String[] splits = s.split("\\.");
        return Arrays.stream(splits)
                .map(s1 ->
                        Utility.intToBytes(Integer.parseInt(s1), 1))
                .reduce((bytes, bytes2) ->
                        concatByteArrays(bytes, bytes2))
                .orElse(new byte[0]);
    }

    public static byte[] generateEncodedBytesForString(String s) {
        if (s == null || s.isEmpty()) return new byte[]{0x00};
        String[] splits = s.split("\\.");
        return concatByteArrays(
                Arrays.stream(splits)
                        .map(s1 ->
                                Utility.concatByteArrays(
                                        Utility.intToBytes(s1.length(), 1),
                                        s1.getBytes(StandardCharsets.UTF_8)
                                ))
                        .reduce((bytes, bytes2) ->
                                Utility.concatByteArrays(bytes, bytes2))
                        .orElse(new byte[0]),
                new byte[]{0x00}
        );
    }

    public static String generateInitialString(int depth) {
        return "\n" + "\t".repeat(Math.max(0, depth));
    }

    public static byte[] concatByteArrays(byte[]... bytes) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            for (int i = 0; i < bytes.length; i++) {
                outputStream.write(bytes[i]);
            }
            return outputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public static <T> T[] concatArrays(T[] first, T[]... rest) {
        int totalLength = first.length;
        for (T[] array : rest) {
            totalLength += array.length;
        }
        T[] result = Arrays.copyOf(first, totalLength);
        int offset = first.length;
        for (T[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

    /**
     * Convert byte array to hex string
     *
     * @param bytes toConvert
     * @return hexValue
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (int idx = 0; idx < bytes.length; idx++) {
            int intVal = bytes[idx] & 0xff;
            if (intVal < 0x10) sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }

    /**
     * Get utf8 byte array.
     *
     * @param str which to be converted
     * @return array of NULL if error was found
     */
    public static byte[] getBytesFromUTF8(String str) {
        try {
            return str.getBytes(StandardCharsets.UTF_8);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getUTF8FromBytes(byte[] bytes) {
        return new String(bytes, StandardCharsets.UTF_8);
    }

}
