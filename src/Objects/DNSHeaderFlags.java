package Objects;

import interfaces.IBytable;
import utils.Utility;

public class DNSHeaderFlags implements IBytable {
    private QuestionResponse QR;
    private Opcode OPCODE;
    private boolean AA;
    private boolean TC;
    private boolean RD;
    private boolean RA;
    private int Z;
    private ResponseCode RCODE;

    public DNSHeaderFlags(byte[] data) {
        this.QR = QuestionResponse.parse(Utility.getBitFromByte(data[0], 0));
        this.OPCODE = Opcode.parse(Utility.getIntFromBitsOfByte(data[0], 1, 5));
        this.AA = Utility.getBitFromByte(data[0], 5);
        this.TC = Utility.getBitFromByte(data[0], 6);
        this.RD = Utility.getBitFromByte(data[0], 7);
        this.RA = Utility.getBitFromByte(data[1], 0);
        this.Z = Utility.getIntFromBitsOfByte(data[1], 1, 3);
        this.RCODE = ResponseCode.parse(Utility.getIntFromBitsOfByte(data[1], 4, 8));
    }


    public QuestionResponse getQR() {
        return QR;
    }

    public DNSHeaderFlags setQR(QuestionResponse QR) {
        this.QR = QR;
        return this;
    }

    public Opcode getOPCODE() {
        return OPCODE;
    }

    public DNSHeaderFlags setOPCODE(Opcode OPCODE) {
        this.OPCODE = OPCODE;
        return this;
    }

    public boolean isAA() {
        return AA;
    }

    public DNSHeaderFlags setAA(boolean AA) {
        this.AA = AA;
        return this;
    }

    public boolean isTC() {
        return TC;
    }

    public DNSHeaderFlags setTC(boolean TC) {
        this.TC = TC;
        return this;
    }

    public boolean isRD() {
        return RD;
    }

    public DNSHeaderFlags setRD(boolean RD) {
        this.RD = RD;
        return this;
    }

    public boolean isRA() {
        return RA;
    }

    public DNSHeaderFlags setRA(boolean RA) {
        this.RA = RA;
        return this;
    }

    public int getZ() {
        return Z;
    }

    public DNSHeaderFlags setZ(int z) {
        Z = z;
        return this;
    }

    public ResponseCode getRCODE() {
        return RCODE;
    }

    public DNSHeaderFlags setRCODE(ResponseCode RCODE) {
        this.RCODE = RCODE;
        return this;
    }

    @Override
    public byte[] generateByteArray() {
//        return new byte[]{(byte) 129, (byte) 128};
        return Utility.toByteArray(
                new boolean[]{(this.QR == null || this.QR.isRawValue())},
                Utility.toBooleanArray(this.OPCODE == null ? 0 : this.OPCODE.getRawValue(), 4),
                new boolean[]{this.AA},
                new boolean[]{this.TC},
                new boolean[]{this.RD},
                new boolean[]{this.RA},
                Utility.toBooleanArray(this.Z, 3),
                Utility.toBooleanArray(this.RCODE == null ? 0 : this.RCODE.getRawValue(), 4)
        );
//        return new byte[0];
    }

    public String toString(int depth) {
        String s = Utility.generateInitialString(depth);
        return s + "DNSHeaderFlags{" +
                s + "QR=" + (QR != null && QR.isRawValue()) +
                s + ", OPCODE=" + OPCODE +
                s + ", AA=" + AA +
                s + ", TC=" + TC +
                s + ", RD=" + RD +
                s + ", RA=" + RA +
                s + ", Z=" + Z +
                s + ", RCODE=" + RCODE +
                s + '}';
    }

    public enum QuestionResponse {
        QUERY(false),
        RESPONSE(true);

        private boolean rawValue;

        QuestionResponse(boolean rawValue) {
            this.rawValue = rawValue;
        }

        static QuestionResponse parse(boolean rawValue) {
            return (rawValue ? RESPONSE : QUERY);
        }

        public boolean isRawValue() {
            return rawValue;
        }
    }

    public enum Opcode {
        QUERY(0), IQUERY(1), STATUS(2), RESERVED(3);

        private int rawValue;

        Opcode(int rawValue) {
            this.rawValue = rawValue;
        }

        public int getRawValue() {
            return rawValue;
        }

        static Opcode parse(int intValue) {
            switch (intValue) {
                case 0:
                    return QUERY;
                case 1:
                    return IQUERY;
                case 2:
                    return STATUS;
                default:
                    return ((intValue >= 3 && intValue <= 15) ? RESERVED : null);
            }
        }
    }

    public enum ResponseCode {
        NO_ERROR(0), FORMAT_ERROR(1), SERVER_FAILURE(2), NAME_ERROR(3), NOT_IMPLEMENTED(4), REFUSED(5);

        private int rawValue;

        ResponseCode(int rawValue) {
            this.rawValue = rawValue;
        }

        public int getRawValue() {
            return rawValue;
        }

        static ResponseCode parse(int intValue) {
            switch (intValue) {
                case 0:
                    return NO_ERROR;
                case 1:
                    return FORMAT_ERROR;
                case 2:
                    return SERVER_FAILURE;
                case 3:
                    return NAME_ERROR;
                case 4:
                    return NOT_IMPLEMENTED;
                case 5:
                    return REFUSED;
                default:
                    return null;
            }
        }
    }
}
