package Objects;

public class ResourceRecord {
    public static ResourceRecord DEFAULT_INSTANCE = new ResourceRecord("DEFAULT_INSTANCE", "94.232.175.55");
    //public static ResourceRecord DEFAULT_INSTANCE = new ResourceRecord("DEFAULT_INSTANCE", "94.184.88.216");
    private String domain;
    private String ipAddress;

    public ResourceRecord() {
        this(null, null);
    }

    public ResourceRecord(String domain, String ipAddress) {
        this.domain = domain;
        this.ipAddress = ipAddress;
    }

    public static ResourceRecord getFromStringLine(String line) {
        if (line == null || line.isEmpty() || line.isBlank()) return null;
        String[] split = line.split(" ");
        if (split.length != 2) return null;
        return new ResourceRecord(split[0], split[1]);
    }

    public String getDomain() {
        return domain;
    }

    public ResourceRecord setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public String getIpAddress() {
        return (ipAddress == null ? "94.232.175.55" : this.ipAddress);
    }

    public ResourceRecord setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }

    public boolean equalsByDomain(String domain) {
        if (domain == null || this.domain == null) return false;
        return this.domain.equals(domain);
    }
}
