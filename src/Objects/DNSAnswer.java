package Objects;

import enums.QueryClass;
import enums.QueryType;
import interfaces.IBytable;
import utils.Utility;

public class DNSAnswer implements IBytable {
    private String NAME;
    private QueryType TYPE;
    private QueryClass CLASS;
    private int TTL;
    private int RDLENGTH;
    private byte[] RDATA;
    private int totalLength;
//    private Object RDATA;


    public DNSAnswer() {
    }

    public DNSAnswer(String NAME, QueryType TYPE, QueryClass CLASS, int TTL, int RDLENGTH, byte[] RDATA, int totalLength) {
        this.NAME = NAME;
        this.TYPE = TYPE;
        this.CLASS = CLASS;
        this.TTL = TTL;
        this.RDLENGTH = RDLENGTH;
        this.RDATA = RDATA;
        this.totalLength = totalLength;
    }

    public DNSAnswer(byte[] data) { //Since we are the server then (probably) never an answer is received!
    }

    public String getNAME() {
        return NAME;
    }

    public DNSAnswer setNAME(String NAME) {
        this.NAME = NAME;
        return this;
    }

    public QueryType getTYPE() {
        return TYPE;
    }

    public DNSAnswer setTYPE(QueryType TYPE) {
        this.TYPE = TYPE;
        return this;
    }

    public QueryClass getCLASS() {
        return CLASS;
    }

    public DNSAnswer setCLASS(QueryClass CLASS) {
        this.CLASS = CLASS;
        return this;
    }

    public int getTTL() {
        return TTL;
    }

    public DNSAnswer setTTL(int TTL) {
        this.TTL = TTL;
        return this;
    }

    public int getRDLENGTH() {
        return RDLENGTH;
    }

    public DNSAnswer setRDLENGTH(int RDLENGTH) {
        this.RDLENGTH = RDLENGTH;
        return this;
    }

    public byte[] getRDATA() {
        return RDATA;
    }

    public DNSAnswer setRDATA(byte[] RDATA) {
        this.RDATA = RDATA;
        return this;
    }

    public int getTotalLength() {
        return totalLength;
    }

    public DNSAnswer setTotalLength(int totalLength) {
        this.totalLength = totalLength;
        return this;
    }

    public String toString(int depth) {
        String s = Utility.generateInitialString(depth);
        return s + "DNSAnswer{" +
                s + "NAME='" + NAME + '\'' +
                s + ", TYPE=" + TYPE +
                s + ", CLASS=" + CLASS +
                s + ", TTL=" + TTL +
                s + ", RDLENGTH=" + RDLENGTH +
                s + ", RDATA=" + (RDATA == null ? "null" : Utility.generateIntStringOfByteArray(RDATA)) +
                s + ", totalLength=" + totalLength +
                s + '}';
    }

    @Override
    public byte[] generateByteArray() {
        return Utility.concatByteArrays(
//                Utility.generateEncodedBytesForString(this.NAME == null ? "" : this.NAME),
                new byte[]{(byte) 192, (byte) 12},  //This is a pointer to the name on the request
                Utility.intToBytes(this.TYPE == null ? 1 : this.TYPE.getRawValue(), 2),
                Utility.intToBytes(this.CLASS == null ? 0 : this.CLASS.getRawValue(), 2),
                Utility.intToBytes(this.TTL, 4),
                Utility.intToBytes(this.RDLENGTH, 2),
                (this.RDATA == null ? new byte[0] : this.RDATA)
        );
    }
}
