package Objects;

import enums.QueryClass;
import enums.QueryType;
import interfaces.IBytable;
import utils.Utility;

import java.util.Arrays;

public class DNSQuestion implements IBytable {
    private String QNAME;
    private QueryType QTYPE;
    private QueryClass QCLASS;
    private transient int totalLength;

    public DNSQuestion(byte[] data) {
        StringBuilder stringBuilder = new StringBuilder();
        int parserCounter = 0;
        int partLength;
        do {
            partLength = Utility.bytesToIntPrime(data[parserCounter++]);
            if (partLength == 0) continue;
            byte[] partBytes = Arrays.copyOfRange(data, parserCounter, parserCounter + partLength);
            String partString = Utility.getUTF8FromBytes(partBytes);
            stringBuilder.append(partString).append('.');
            parserCounter += partLength;
        } while (partLength != 0);
        if (stringBuilder.length() > 0) stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        this.QNAME = stringBuilder.toString();
        /////////////////////////
        this.QTYPE = QueryType.parse(Utility.bytesToIntPrime(Arrays.copyOfRange(data, parserCounter, ++parserCounter + 1)));
        this.QCLASS = QueryClass.parse(Utility.bytesToIntPrime(Arrays.copyOfRange(data, ++parserCounter, ++parserCounter + 1)));
        this.totalLength = ++parserCounter;
    }

    public String getQNAME() {
        return QNAME;
    }

    public DNSQuestion setQNAME(String QNAME) {
        this.QNAME = QNAME;
        return this;
    }

    public QueryType getQTYPE() {
        return QTYPE;
    }

    public DNSQuestion setQTYPE(QueryType QTYPE) {
        this.QTYPE = QTYPE;
        return this;
    }

    public QueryClass getQCLASS() {
        return QCLASS;
    }

    public DNSQuestion setQCLASS(QueryClass QCLASS) {
        this.QCLASS = QCLASS;
        return this;
    }

    public int getTotalLength() {
        return totalLength;
    }

    public DNSQuestion setTotalLength(int totalLength) {
        this.totalLength = totalLength;
        return this;
    }

    public String toString(int depth) {
        String s = Utility.generateInitialString(depth);
        return s + "DNSQuestion{" +
                s + "QNAME='" + QNAME + '\'' +
                s + ", QTYPE=" + QTYPE +
                s + ", QCLASS=" + QCLASS +
                s + '}';
    }

    @Override
    public byte[] generateByteArray() {
        return Utility.concatByteArrays(
                Utility.generateEncodedBytesForString(this.QNAME == null ? "" : this.QNAME),
                Utility.intToBytes(this.QTYPE == null ? 0 : this.QTYPE.getRawValue(), 2),
                Utility.intToBytes(this.QCLASS == null ? 0 : this.QCLASS.getRawValue(), 2)
        );
//        return new byte[0];
    }
}

