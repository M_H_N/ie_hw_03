package Objects;


import interfaces.IBytable;
import utils.Utility;

import java.util.Arrays;

public class DNSHeader implements IBytable {
    private int ID;
    private DNSHeaderFlags FLAGS;
    private int QDCOUNT;
    private int ANCOUNT;
    private int NSCOUNT;
    private int ARCOUNT;

    public DNSHeader(byte[] data) {
        this.ID = Utility.bytesToIntPrime(Arrays.copyOfRange(data, 0, 2));
        this.FLAGS = new DNSHeaderFlags(Arrays.copyOfRange(data, 2, 4));
        this.QDCOUNT = Utility.bytesToIntPrime(Arrays.copyOfRange(data, 4, 6));
        this.ANCOUNT = Utility.bytesToIntPrime(Arrays.copyOfRange(data, 6, 8));
        this.NSCOUNT = Utility.bytesToIntPrime(Arrays.copyOfRange(data, 8, 10));
        this.ARCOUNT = Utility.bytesToIntPrime(Arrays.copyOfRange(data, 10, 11));
    }


    public int getID() {
        return ID;
    }

    public DNSHeader setID(int ID) {
        this.ID = ID;
        return this;
    }

    public DNSHeaderFlags getFLAGS() {
        return FLAGS;
    }

    public DNSHeader setFLAGS(DNSHeaderFlags FLAGS) {
        this.FLAGS = FLAGS;
        return this;
    }

    public int getQDCOUNT() {
        return QDCOUNT;
    }

    public DNSHeader setQDCOUNT(int QDCOUNT) {
        this.QDCOUNT = QDCOUNT;
        return this;
    }

    public int getANCOUNT() {
        return ANCOUNT;
    }

    public DNSHeader setANCOUNT(int ANCOUNT) {
        this.ANCOUNT = ANCOUNT;
        return this;
    }

    public int getNSCOUNT() {
        return NSCOUNT;
    }

    public DNSHeader setNSCOUNT(int NSCOUNT) {
        this.NSCOUNT = NSCOUNT;
        return this;
    }

    public int getARCOUNT() {
        return ARCOUNT;
    }

    public DNSHeader setARCOUNT(int ARCOUNT) {
        this.ARCOUNT = ARCOUNT;
        return this;
    }

    public String toString(int depth) {
        String s = Utility.generateInitialString(depth);
        return s + "DNSHeader{" +
                s + "ID=" + ID +
                s + ", FLAGS=" + FLAGS.toString(depth + 1) +
                s + ", QDCOUNT=" + QDCOUNT +
                s + ", ANCOUNT=" + ANCOUNT +
                s + ", NSCOUNT=" + NSCOUNT +
                s + ", ARCOUNT=" + ARCOUNT +
                s + '}';
    }

    @Override
    public byte[] generateByteArray() {
        return Utility.concatByteArrays(
                Utility.intToBytes(this.ID, 2),
                (this.FLAGS == null ? new byte[0] : this.FLAGS.generateByteArray()),
                Utility.intToBytes(this.QDCOUNT, 2),
                Utility.intToBytes(this.ANCOUNT, 2),
                Utility.intToBytes(this.NSCOUNT, 2),
                Utility.intToBytes(this.ARCOUNT, 2)
        );
    }
}
