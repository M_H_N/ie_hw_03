package Objects;

import interfaces.IBytable;
import utils.Utility;

import static java.util.Arrays.copyOfRange;

public class DNSPacket implements IBytable {
    private DNSHeader HEADER;
    private DNSQuestion QUESTION;
    private DNSAnswer ANSWER;
    private DNSAuthority AUTHORITY;
    private DNSAdditional ADDITIONAL;

    public DNSPacket(byte[] data) {
        this.HEADER = new DNSHeader(copyOfRange(data, 0, 12));
        this.QUESTION = (this.HEADER.getQDCOUNT() == 0 ? null : new DNSQuestion(copyOfRange(data, 12, data.length)));
        int questionLength = (this.QUESTION == null ? 0 : this.QUESTION.getTotalLength());
        this.ANSWER = (this.HEADER.getANCOUNT() == 0 ? null : new DNSAnswer(copyOfRange(data, 12 + questionLength, data.length)));
        int answerLength = (this.ANSWER == null ? 0 : this.ANSWER.getTotalLength());
//        this.authority = (this.header.getARCOUNT() == 0 ? null : new DNSAuthority());
        this.AUTHORITY = null;
//        this.additional = (this.header.getARCOUNT() == 0 ? null : new DNSAdditional());
        this.ADDITIONAL = null;
    }

    public DNSHeader getHEADER() {
        return HEADER;
    }

    public DNSPacket setHEADER(DNSHeader HEADER) {
        this.HEADER = HEADER;
        return this;
    }

    public DNSQuestion getQUESTION() {
        return QUESTION;
    }

    public DNSPacket setQUESTION(DNSQuestion QUESTION) {
        this.QUESTION = QUESTION;
        return this;
    }

    public DNSAnswer getANSWER() {
        return ANSWER;
    }

    public DNSPacket setANSWER(DNSAnswer ANSWER) {
        this.ANSWER = ANSWER;
        return this;
    }

    public DNSAuthority getAUTHORITY() {
        return AUTHORITY;
    }

    public DNSPacket setAUTHORITY(DNSAuthority AUTHORITY) {
        this.AUTHORITY = AUTHORITY;
        return this;
    }

    public DNSAdditional getADDITIONAL() {
        return ADDITIONAL;
    }

    public DNSPacket setADDITIONAL(DNSAdditional ADDITIONAL) {
        this.ADDITIONAL = ADDITIONAL;
        return this;
    }

    public String toString(int depth) {
        String s = Utility.generateInitialString(depth);
        return s + "DNSPacket{" +
                s + "HEADER=" + (HEADER == null ? "null" : HEADER.toString(depth + 1)) +
                s + ", QUESTION=" + (QUESTION == null ? "null" : QUESTION.toString(depth + 1)) +
                s + ", ANSWER=" + (ANSWER == null ? "null" : ANSWER.toString(depth + 1)) +
                s + ", AUTHORITY=" + (AUTHORITY == null ? "null" : AUTHORITY.toString(depth + 1)) +
                s + ", ADDITIONAL=" + (ADDITIONAL == null ? "null" : ADDITIONAL.toString(depth + 1)) +
                s + '}';
    }


    @Override
    public byte[] generateByteArray() {
        return Utility.concatByteArrays(
                (this.HEADER == null ? new byte[0] : this.HEADER.generateByteArray()),
                (this.QUESTION == null ? new byte[0] : this.QUESTION.generateByteArray()),
                (this.ANSWER == null ? new byte[0] : this.ANSWER.generateByteArray()),
                (this.AUTHORITY == null ? new byte[0] : this.AUTHORITY.generateByteArray()),
                (this.ADDITIONAL == null ? new byte[0] : this.ADDITIONAL.generateByteArray())
        );
    }
}
